const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");

router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.get("/decrypt", (req, res) => {
    userController.decrypt(req.body).then(resultFromController => res.send(resultFromController))
});

router.get("/emailAttemps", (req, res) => {
    userController.countIpAddress(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/blacklist-email", (req, res) => {
    userController.blacklistEmail(req.body).then(resultFromController => res.send(resultFromController))
});

router.get("/check-blacklist", (req, res) => {
    userController.checkBlackListed(req.body).then(resultFromController => res.send(resultFromController))
});

router.get("/check-user", (req, res) => {
    userController.checkUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/add-message", (req, res) => {
    userController.addMessage(req.body).then(resultFromController => res.send(resultFromController))
});


module.exports = router;