const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "email is required"] 
	},
	contacts : [
		{
			website : {
			type: String,
			required : [true, "website is required"]
		   },
			concern : {
			type: String,
			required : [true, "concern is required"]
		   },
			date : {
			type: Date,
			default : new Date()
			}, 
		   	country : {
			type : String,
			required: [true, "country is required"]
			},
			city : {
				type : String,
				required: [true, "city is required"]
			},
			ipAddress : {
				type : String,
				required: [true, "city is required"]
			},
		}
	],
	// date : {
	// 	type: Date,
	// 	default : new Date()
	// }
},{ timestamps: true });


module.exports = mongoose.model("User", userSchema);

